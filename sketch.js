var p, g;
var keys = [];

function setup() {
  createCanvas(400, 400);
  textSize(30);
  p = new Player();
  g = new Goal();
}

function draw() {
  background(100);
  p.show();
  p.update();
  g.show();
  p.checkGoal(g);
  
  fill(255);
  text(p.score, width - 40, 40);
}

function keyPressed() {
  keys[keyCode] = true;
}
function keyReleased() {
  keys[keyCode] = false;
}

function Goal() {
  this.x = 50;
  this.y = 50;
  this.size = 15;
  
  this.makeNewPosition = function() {
    this.x = random(0+this.size/2, width-this.size/2);
    this.y = random(0+this.size/2, height-this.size/2);
  }
  
  this.show = function() {
    fill(255, 230, 0);
    ellipse(this.x, this.y, this.size, this.size);
  }
}

function Player() {
  this.x = 20;
  this.y = 20;
  this.size = 30;
  this.xv = 0;
  this.yv = 0;
  this.accel = 0.3;
  this.deccel = 0.96;
  this.score = 0;
  
  this.checkGoal = function(goal) {
    var d = dist(this.x, this.y, goal.x, goal.y);
    if (d < this.size/2 + goal.size/2) {
      goal.makeNewPosition();
      this.score++;
    }
  }
  
  this.update = function() {
    
    // update velocities
    if (keys[UP_ARROW] === true)
      this.yv -= this.accel;
    if (keys[RIGHT_ARROW] === true)
      this.xv += this.accel;
    if (keys[DOWN_ARROW] === true)
      this.yv+= this.accel;
    if (keys[LEFT_ARROW] === true)
      this.xv -= this.accel;
    
    // check wall collisions
    if (this.x + this.size/2 > width)
      this.xv *= -1;
    if (this.x - this.size/2 < 0)
      this.xv *= -1;
    if (this.y + this.size/2 > height)
      this.yv *= -1;
    if (this.y - this.size/2 < 0)
      this.yv *= -1;
    
    
    this.xv *= this.deccel;
    this.yv *= this.deccel;
    
    this.x = this.x + this.xv;
    this.y = this.y + this.yv;
    
  }
  
  this.show = function() {
    fill(255);
    ellipse(this.x, this.y, this.size, this.size);
  }
}