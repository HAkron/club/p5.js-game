# P5.js Game Tutorial

p5.js is a Javascript framework that makes the complex parts of Javascript not so complex, adding lots of functions that work how you'd expect them to. It works very similarly to Processing, a software sketchbook used primarily for programming with a visual output. Much like other programming frameworks (Arduino, Processing), there is a "setup()" function which runs once, and a "draw()" function which runs forever (similar to loop() in Arduino). Let's use these to make a fun little game!

### Basic Editor Layout & Functionality

In the [web editor](https://editor.p5js.org), it's extremely easy to get up and running with the p5.js framework. As you can see it starts you off with the setup and draw functions we talked about. If you click the play button, you'll see a lovely gray box show up without having to code anything! Neat! 

### Making a simple Game

In the meeting, we'll be using the [web editor](https://editor.p5js.org) to code [these steps](https://gitlab.com/HAkron/club/p5.js-game/blob/master/steps.pdf), which I will break down in some detail here-

##### Step 1
In this step, all we are going to do is create an object by making a function. In this object, we'll give it an x and y position, and a size. We'll also put a function within that function and make a call to it in the main draw() function that will dislpay our lovely Player object to the screen. This "show()" function sets the color of the inside of shapes to 255 which is white, then draws an ellipse based on the position and size of the Player. 

#### Step 2
Here we're going to make this Player move. To do this, we need to make another function, "update()" in the Player object. In it, we'll update the players position based on new proeprties we're going to give the player, which are x and y velocities. We'll also have to call this function in draw() to make sure our program actually uses the code we just wrote.

#### Step 3
Now let's listen for keypresses (and releases) to control this Player's speed. To do this, we're going to utilize the built in niceness of p5.js, writing the functions "keyPressed()" and "keyReleased()" to set boolean values in an array we're going to define as "keys"

We're also going to add checks in the Player object for if certain keys are pressed, then to update the velocities based on which keys are pressed. So if the left key is being pressed down, we'll want to increment vx, moving the player to the left. We can add an acceleration property to the player to help define how much speed these keypresses give us.

Also also, we want to make this game semi-realistic and have the player slow down to a stop over time (due to friction or something like that). To do this, we can constantly multiply the velocity by a number slightly less than 1. Let's make a new player property for deceleration and add that multiplication step.

#### Step 4
Making our player fly off the edge is way too easy, so let's add some wall collisions! The goal is to make the player bounce off the wall when it hits it. To accomplish this, we'll add some position checks in the player's update function. 

All it means for a player to hit a wall is that the position is greater than the width of the canvas. We also have to account for the size of the player, since the position is actually the center of that circle. Meaning, when the right part of the player hits the wall is when p.x === width - p.size/2. We can add similar checks for the bottom, left, and top wall. 

#### Step 5
The movement works great! Now let's add a little something for our player to collect: little coin/goal object things! 

We'll make another function object similar to Player, and give it values of x, y, and size. It doesn't need a velocity or any of that stuff since it won't be moving. We'll also whip up a show function to tell the canvas what to draw. Again, we'll make an ellipse, but instead set fill to a nice golden yellowish RGB value of (255, 223, 0). Don't forget to make the variable for the goal, initialize like we did the Player in the setup function, and then make a call to it in draw so we can see it. 

#### Step 6
What a pretty golden ball! But we can't touch it yet D: Now, we'll add collision detection between the player and the goal. To do this, we'll make a new function in the Player object called checkGoal(). We'll have to pass the goal object into it so we can see it's position in the function. 

In this checkGoal function, we'll use the built-in dist() function to get the distance between the center of the player and the center of the goal. We need to make a check if the distance between them is less than the sum of their radii. Think about it - the smallest line we can draw between them without them intersecting each other is the sum of the two radii. 

So let's check for that - if the distance between the centers is less than the sum of their radii, then they've intersected! We need to make that goal disappear and generate a new one somewhere else after our player has "collected" it. To do this, we need to randomly generate a new position in the Goal object. 

We'll make a new function called makeNewPosition(), and set the x and y coordinates to new random variables that are inside the canvas. If we put 2 arguments into the random() function that p5 gives us, it'll generate a random number between the two. So we can get a random number between 0 and the width of the canvas and set that to the new x position, and then a number between 0 and the height for the new y position. Presto Goal-o!

#### Step 7
What fun is collecting anything if you don't see numbers go up?

Let's add a score property to the player object and set it initially to 0 since we all start out in life with nothing. We can increment this score whenever our checkGoal condition passes. We'll need to display this score, too. To do this, we can call the text() function in draw(), and pass it the text we want to display, and an x/y position to display it on the screen. Slightly adjust the text size and color and BAM we have a finished product!!


During the meeting, I plan to finish this a bit early and free-code some functionality like instead of bouncing off the walls, teleporting to the opposite side of the screen, making a shrinking goal to make the game more fun, or any other ideas the audience gives me. Thanks for reading!